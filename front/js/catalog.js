"use strict";
const loadDiv = document.getElementById("loadMore");
function init() {
  let page = 1;
  loadCatalog(page);
  // showButtonLoadMore(true);

  loadDiv.addEventListener("click", () => {
    disableButtonLoadMore();
    page++;
    loadCatalog(page);
    enableButtonLoadMore();
  });
  // вызови функцию loadCatalog для загрузки первой страницы каталога
  // Реализуй и установи обработчик нажатия на кнопку "Загрузить еще"
}

function loadCatalog(page) {
  console.log(getPointId());
  api.getBikes(getPointId(), page).then((bikeList) => {
    console.log(bikeList);
    appendCatalog(bikeList);
    showButtonLoadMore(bikeList.hasMore);
  });

  // Здесь необходимо сделать загрузку каталога (api.getBikes)
  // и передать полученные данные в функции appendCatalog и showButtonLoadMore
}

function appendCatalog(items) {
  // отрисуй велосипеды из items в блоке <div id="bikeList">
  console.log(items.bikesList);
  const node = document.getElementById("bikeList");
  for (let item of items.bikesList) {
    const bikeDiv = document.createElement("div");
    bikeDiv.classList.add("bike");
    const bikeImg = document.createElement("div");
    bikeImg.classList.add("bike_image");
    const bikeDescription = document.createElement("div");
    bikeDescription.innerHTML = item.name;
    bikeDescription.classList.add("bike_description");
    const bikePrice = document.createElement("div");
    bikePrice.innerHTML = `Стоимость за час - ${item.cost} ₽`;
    bikePrice.classList.add("bike_price");
    const image = document.createElement("img");
    image.src = "../images/" + item.img;
    bikeImg.append(image);
    const bikeButton = document.createElement("button");
    bikeButton.innerHTML = "Арендовать";
    bikeButton.classList.add("rent_button");
    bikeDiv.append(bikeImg);
    bikeDiv.append(bikeDescription);
    bikeDiv.append(bikePrice);
    bikeDiv.append(bikeButton);
    node.append(bikeDiv);
  }
}

function showButtonLoadMore(hasMore) {
  // если hasMore == true, то показывай кнопку #loadMore

  if (hasMore) {
    console.log("Попал в иф");

    loadDiv.classList.replace("hidden", "visible");
  } else {
    loadDiv.classList.replace("visible", "hidden");
  }
  // иначе скрывай
}

function disableButtonLoadMore() {
  // заблокируй кнопку "загрузить еще"
  loadDiv.disabled = true;
}

function enableButtonLoadMore() {
  // разблокируй кнопку "загрузить еще"
  loadDiv.disabled = false;
}

function getPointId() {
  const url = document.URL;
  console.log("url: ", url);
  const splittedUrl = url.split("/");
  // сделай определение id выбранного пункта проката
  return splittedUrl[splittedUrl.length - 1];
}

document.addEventListener("DOMContentLoaded", init);
